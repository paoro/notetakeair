import unittest

import evernote_proxy_test
import notetakeair_test

test_suits = unittest.TestSuite([
    unittest.TestLoader().loadTestsFromModule(notetakeair_test),
    unittest.TestLoader().loadTestsFromModule(evernote_proxy_test),
])

unittest.TextTestRunner(verbosity=2).run(test_suits)
