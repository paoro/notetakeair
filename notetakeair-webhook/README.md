# App Engine Standard Flask Hello World

This sample shows how to use [Flask](http://flask.pocoo.org/) with Google App
Engine Standard.

Before running or deploying this application, install the dependencies using
[pip](http://pip.readthedocs.io/en/stable/):

    pip install -r requirements.txt
    pip install -t lib -r requirements.txt
    
To request a Evernote development token go to [here](https://sandbox.evernote.com/api/DeveloperToken.action) 

To run the application you can either use the flask command or python’s -m switch with Flask. Before you can do that you need to tell your terminal the application to work with by exporting the FLASK_APP environment variable:

    python notetakeair/app.py
    
To test your application locally (uses mock Evernote Client):
    
    python unit_test.py
    
To validate Evernote notebooks in development environment go to [Evernote Sandboc](https://sandbox.evernote.com/)

To deploy your app to App Engine, run the following command from within the root directory of your application where the app.yaml file is located:

    gcloud app deploy --project paoro-168419
    
Service name (notetakeair-webhook-core) is included into the `app.yaml` file.
      
To view your application in the web browser run:
    
    gcloud app browse --project paoro-168419 --service notetakeair-webhook-core

For more information, see the [App Engine Standard README](../../README.md)
