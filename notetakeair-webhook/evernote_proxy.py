# Copyright (C) 2017 Paoro
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from evernote.api.client import EvernoteClient
from evernote.api.client import NoteStore
import evernote.edam.type.ttypes as Types
from xml.etree import ElementTree


class EvernoteProxy:
    def __init__(self, authentication_token):
        self.authentication_token = authentication_token
        self.client = EvernoteClient(token=self.authentication_token)
        self.user_store = self.client.get_user_store()
        self.note_store = self.client.get_note_store()

    def open_note(self, note_title):

        note_filter = NoteStore.NoteFilter()
        note_filter.words = '"%s"' % note_title

        spec = NoteStore.NotesMetadataResultSpec()
        spec.includeTitle = True

        note_list = self.note_store.findNotesMetadata(self.authentication_token, note_filter, 0, 100, spec)
        note_list = [note for note in note_list.notes if note.title == note_title]

        if len(note_list) > 0:
            my_shopping_list = note_list[0]
            my_shopping_list = self.note_store.getNote(self.authentication_token,
                                                       my_shopping_list.guid,
                                                       True, False, False, False)
            return my_shopping_list
        else:
            return None

    def create_note(self, note_title, note_content):
        note = Types.Note()
        note.title = note_title
        note.content = enml_create_note_content(note_content)

        note = self.note_store.createNote(self.authentication_token, note)

        return note

    def add_item_to_list_note(self, note, item):
        note.content = enml_add_item_to_list_note_content(note.content, item)
        note = self.note_store.updateNote(self.authentication_token, note)
        note = self.note_store.getNote(self.authentication_token,
                                       note.guid,
                                       True, False, False, False)
        return note


def enml_create_note_content(note_content):
    """    
    Prepares the content with the required xml version, doctype and root element. 
    :param note_content: the content as content in HTML (html as string) 
    :return: the content as string formatted in Evernote Markup Language (xml as string)  
    """
    evernote_note_content = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
    evernote_note_content += "<!DOCTYPE en-note SYSTEM \"http://xml.evernote.com/pub/enml2.dtd\">"
    evernote_note_content += "<en-note>%s</en-note>" % note_content
    return evernote_note_content


def enml_add_item_to_list_note_content(note_content, item):
    """    
    :param note_content: note content as string formatted in Evernote Markup Language (xml as string)  
    :param item: a single item to be added to the content (string)
    :return: the content as string formatted in Evernote Markup Language (xml as string)  
    """
    root = ElementTree.fromstring(note_content)
    if root.tag != "en-note":
        raise Warning("content root is expected to be <en-note> but it is %s" % root.tag)
    ul = root.find("ul")
    if ul is None:
        ul = ElementTree.SubElement(root, "ul")
    li = ElementTree.SubElement(ul, "li")
    li.text = item

    note_content = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
    note_content += "<!DOCTYPE en-note SYSTEM \"http://xml.evernote.com/pub/enml2.dtd\">"
    note_content += ElementTree.tostring(root)
    return note_content
