# Copyright (C) 2017 Paoro
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import unittest
import evernote_proxy
import uuid
from evernote.api.client import EvernoteClient


class EvernoteProxyTestCase(unittest.TestCase):
    doctype = '<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE en-note SYSTEM "http://xml.evernote.com/pub/enml2.dtd">'

    def test_enml_add_item_to_list_note_none(self):
        """
        GIVEN a list note without <ul> element found in content
        WHEN a new item is added to the list note
        THEN a <ul> element is created as the last element of <en-note>
        AND the item is added
        """
        content = self.doctype + '<en-note><p>paragraph element</p></en-note>'
        item = "new item"
        actual = evernote_proxy.enml_add_item_to_list_note_content(content, item)
        expected = self.doctype + '<en-note><p>paragraph element</p><ul><li>new item</li></ul></en-note>'
        self.assertEqual(actual, expected)

    def test_enml_add_item_to_list_note_one(self):
        """
        GIVEN a list note with one <ul> element found in content
        WHEN a new item is added to the list note
        THEN the item is appended to <ul> element as the last <li> element
        """
        content = self.doctype + '<en-note><p>paragraph element</p><ul><li>existing item</li></ul></en-note>'
        item = "new item"
        actual = evernote_proxy.enml_add_item_to_list_note_content(content, item)
        expected = self.doctype + '<en-note><p>paragraph element</p><ul><li>existing item</li><li>new item</li></ul></en-note>'
        self.assertEqual(actual, expected)

    def test_enml_add_item_to_list_note_multiple(self):
        """
        GIVEN a list note with multiple <ul> element found in content
        WHEN a new item is added to the list note
        THEN the item is appended to the first <ul> element as the last <li> element
        """
        content = self.doctype + '<en-note><p>paragraph element</p><ul><li>existing item</li></ul><ul><li>second list</li></ul></en-note>'
        item = "new item"
        actual = evernote_proxy.enml_add_item_to_list_note_content(content, item)
        expected = self.doctype + '<en-note><p>paragraph element</p><ul><li>existing item</li><li>new item</li></ul><ul><li>second list</li></ul></en-note>'
        self.assertEqual(actual, expected)


class EvernoteClientMock(EvernoteClient):
    def get_user_store(self):
        return UserStoreMock()

    def get_note_store(self):
        return NoteStoreMock()


class UserStoreMock:
    pass


class NoteStoreMock(dict):
    def findNotesMetadata(self, authenticationToken, filter, offset, maxNotes, resultSpec):
        return NoteFindResult([note for note in self.values() if note.title in filter.words])

    def createNote(self, authenticationToken, note):
        note.guid = str(uuid.uuid4())
        self[note.guid] = note
        return self.get(note.guid)

    def getNote(self, authenticationToken, guid,
                withContent, withResourcesData, withResourcesRecognition, withResourcesAlternateData):
        return self.get(guid)

    def updateNote(self, authenticationToken, note):
        if note.guid in self:
            self[note.guid] = note
            return self.get(note.guid)
        else:
            raise Exception("note does not exist")


class NoteFindResult:
    def __init__(self, notes):
        self.notes = notes


if __name__ == '__main__':
    unittest.main()
