# Copyright (C) 2017 Paoro
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from apiaiwebhook import APIAIWebhook

from evernote_proxy import EvernoteProxy

app = APIAIWebhook(__name__, api_key_value="f0858d6b-1188-4d61-a20b-3e6bfbadfb6a")
proxy = EvernoteProxy(
    authentication_token="S=s1:U=93be3:E=163e9836263:C=15c91d23338:P=1cd:A=en-devtoken:V=2:H=f4ccee0efe7f001ed5ef3d069dbf406d")


@app.fulfillment("add-item-to-my-shopping-list")
def add_item_to_my_shopping_list(item):
    # open or create shopping list
    note_title = "My Shopping List"
    note = proxy.open_note(note_title)
    if note is None:
        app.logger.debug("%s is not found" % note_title)
        note = proxy.create_note(note_title, "")
        app.logger.debug("%s is created: %s" % (note.title, note.guid))

        app.logger.debug("(before) %s: %s" % (note.title, note.content))

    note = proxy.add_item_to_list_note(note, item)

    app.logger.debug("(after) %s: %s" % (note.title, note.content))

    # build response
    speech = "%s is added to your shopping list (python webhook)" % item
    return app.make_response_apiai(speech=speech, display_text=speech)


if __name__ == '__main__':
    app.run()
