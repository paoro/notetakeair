# Copyright (C) 2017 Paoro
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import unittest

import mock

from evernote_proxy_test import EvernoteClientMock


class NotetakeairTest(unittest.TestCase):
    def setUp(self):
        import notetakeair
        notetakeair.app.testing = True
        notetakeair.app.debug = True
        self.test_client = notetakeair.app.test_client_apiai()

    @mock.patch("evernote_proxy.EvernoteClient", EvernoteClientMock)
    def test_add_item_to_my_shopping_list(self):
        r = self.test_client.webhook(result_action="add-item-to-my-shopping-list",
                                     result_parameters={
                                         "item": "half a bunch of fresh flat-leaf parsley"
                                     })
        assert r.status_code == 200
        assert "half a bunch of fresh flat-leaf parsley is added to your shopping list" in r.data.decode("utf-8")


if __name__ == '__main__':
    unittest.main()
