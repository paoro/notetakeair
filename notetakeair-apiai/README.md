### Export as Zip
1. Open the project [settings](https://console.api.ai/api-client/#/editAgent/2413cfc2-0219-45f8-8378-f223af06d160/)
1. Select "Export and Import" tab
1. Click on "EXPORT AS ZIP" button
1. Extract the file to the "notetakeair/notetakeair-apiai" folder

### Restore from Zip
1. Archive all files under the "notetakeair/notetakeair-apiai" folder
1. Open the project [settings](https://console.api.ai/api-client/#/editAgent/2413cfc2-0219-45f8-8378-f223af06d160/)
1. Select "Export and Import" tab
1. Click on "RESTORE FROM ZIP" button
